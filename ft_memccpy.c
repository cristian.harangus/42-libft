/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 12:16:41 by cristi            #+#    #+#             */
/*   Updated: 2017/09/27 14:26:45 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	char		*temp_dest;
	const char	*temp_src;
	size_t		i;
	
	temp_dest = (char *)dest;
	temp_src = (const char*)src;
	i = 0;

	while(i < n && *(temp_src + i) != c)
	{
		*(temp_dest + i) = *(temp_src + i);
		i++;
	}
	if (i < n)
		*(temp_dest + i) = *(temp_src + i);
	return(dest);
}
