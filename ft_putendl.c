/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/27 11:45:24 by charangu          #+#    #+#             */
/*   Updated: 2017/11/27 11:51:16 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_putchar.c"

void ft_putendl(char const *s)
{
	while (*s)
		ft_putchar(*s++);
	ft_putchar('\n');
}

//int main()
//{
//	char s1[] = "this is some line";
//	char *s2 = "this is another line";
//	ft_putendl(s1);
//	ft_putendl(s2);
//	return(0);
//}
