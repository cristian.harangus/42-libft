/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 13:53:12 by cristi            #+#    #+#             */
/*   Updated: 2017/09/27 14:42:16 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlen(const char *str)
{
	size_t i;

	i = 0;
	while(*(str + i))
		i++;
	return(i);
}

char 		*ft_strncat(char *dest, const char *src, size_t n)
{
	
	char *temp;

	temp = dest;
	while(*temp != '\0')
		temp++;
	while(n > 0 && *src != '\0')
	{
		*temp = *src;
	}
	*temp = '\0';
	return(dest);
}