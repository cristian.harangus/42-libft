/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/27 11:51:36 by charangu          #+#    #+#             */
/*   Updated: 2017/11/27 12:04:51 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_putstr.c"
//#include "ft_putchar.c"

void ft_putnbr(int n)
{
	if (n == -2147483648)
	{
		ft_putstr("-2147483648");
	}
	else if(n<0)
	{
		ft_putstr("-");
		n *= -1;
	}

	if (n<10)
	{
		ft_putchar(n + '0');
	}
	else
	{
		ft_putnbr(n/10);
		ft_putnbr(n%10);
	}
}

//int main()
//{
//	ft_putnbr(71326);
//	ft_putchar('\n');
//	ft_putnbr(-192394);
//	ft_putchar('\n');
//	ft_putnbr(-2147483648);
//	ft_putchar('\n');
//	ft_putnbr(7812460144618734);
//}
