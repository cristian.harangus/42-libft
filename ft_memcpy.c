/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 04:56:30 by cristi            #+#    #+#             */
/*   Updated: 2017/09/27 14:23:08 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memcpy(void *dest, const void *src, size_t n)
{
    char *temp_dest;
    const char *temp_src;
    size_t i;

    temp_dest = (char *)dest;
    temp_src = (const char*)src;
    i = 0;

    while(i<n)
    {
        *(temp_dest + i) = *(temp_src + i);
        i++;
    }
    return(dest);
}
