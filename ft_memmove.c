/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 12:27:45 by cristi            #+#    #+#             */
/*   Updated: 2017/09/25 13:11:00 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
// #include "ft_memcpy.c"

void		*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char temp[n];

	ft_memcpy(temp, src, n);
	ft_memcpy(dest, temp, n);
	return(dest);
}
