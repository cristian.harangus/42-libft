/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <charangu@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/29 18:12:42 by charangu          #+#    #+#             */
/*   Updated: 2017/09/30 00:21:12 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strstr(const char *dest, const char *src)
{
	size_t		i;
	char		*ft_dest;
	char		*ft_src;
	int			length;
	
	if (*src == '\0')
		return ((char *)dest);
	ft_dest = (char *)dest;
	ft_src = (char *)src;
	
}

int m ()
{
  char str[] ="This is a simple string";
  char * pch;
  pch = strstr (str,"simple");
  strncpy (pch,"sample",6);
  puts (str);
  return 0;
}

int ftm ()
{
  char str[] ="This is a simple string";
  char * pch;
  pch = strstr (str,"simple");
  strncpy (pch,"sample",6);
  puts (str);
  return 0;
}

int main(void)
{
	m();
	ftm();
	return(0);
}