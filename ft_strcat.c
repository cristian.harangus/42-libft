/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <charangu@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 12:15:39 by charangu          #+#    #+#             */
/*   Updated: 2017/09/28 14:25:34 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"
#include "./ft_strlen.c"

char		*ft_strcat(char *dest, const char *src)
{
	char *temp;
	int n;
	
	temp = dest;
	n = ft_strlen(src);
	while(*temp != '\0')
		temp++;
	while(n > 0 && *src != '\0')
	{
		*temp = *src;
		n --;
	}
	return(dest);
}

// int main ()
// {
// 	char src[50], dest[50];
// 	strcpy(src,  "This is source");
// 	strcpy(dest, "This is destination");
// 	strcat(dest, src);
// 	printf("Final destination string : |%s|", dest);
// 	printf("\n");
// 	char ft_src[50], ft_dest[50];
// 	strcpy(ft_src,  "This is source");
// 	strcpy(ft_dest, "This is destination");
// 	strcat(ft_dest, ft_src);
// 	printf("Final destination string : |%s|", ft_dest);
	
// 	return(0);
// }