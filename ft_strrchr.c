/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <charangu@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/29 16:44:50 by charangu          #+#    #+#             */
/*   Updated: 2017/09/29 16:52:15 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrchr(const char *s, int c)
{
	int ok;

	ok = 0;
	while (*s)
	{
		if (*s == c)
			ok = 1;
		s++;
	}
	if (ok == 0)
		return (NULL);
	while(*s != c)
		s--;
	return((char *)s);
}

int m (void)
{
  char str[] = "This is a sample string";
  char * pch;
  printf ("Looking for the 's' character in \"%s\"...\n",str);
  pch=strrchr(str,'s');
  while (pch!=NULL)
  {
    printf ("found at %d\n",(int)(pch-str+1));
    pch=strrchr(pch+1,'s');
  }
  return 0;
}

int ft_m (void)
{
  char str[] = "This is a sample string";
  char * pch;
  printf ("Looking for the 's' character in \"%s\"...\n",str);
  pch=ft_strrchr(str,'s');
  while (pch!=NULL)
  {
    printf ("found at %d\n",(int)(pch-str+1));
    pch=ft_strrchr(pch+1,'s');
  }
  return 0;
}

int main(void)
{
	m();
	ft_m();
	return(0);
}