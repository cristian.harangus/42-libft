/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <charangu@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/29 16:32:09 by charangu          #+#    #+#             */
/*   Updated: 2017/09/29 16:44:33 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strchr(const char *s, int c)
{
	while(*s)
	{
		if(*s == c)
			return((char *)s);
		s++;
	}
	return (NULL);
}

// int m (void)
// {
//   char str[] = "This is a sample string";
//   char * pch;
//   printf ("Looking for the 's' character in \"%s\"...\n",str);
//   pch=strchr(str,'s');
//   while (pch!=NULL)
//   {
//     printf ("found at %d\n",(int)(pch-str+1));
//     pch=strchr(pch+1,'s');
//   }
//   return 0;
// }

// int ft_m (void)
// {
//   char str[] = "This is a sample string";
//   char * pch;
//   printf ("Looking for the 's' character in \"%s\"...\n",str);
//   pch=ft_strchr(str,'s');
//   while (pch!=NULL)
//   {
//     printf ("found at %d\n",(int)(pch-str+1));
//     pch=ft_strchr(pch+1,'s');
//   }
//   return 0;
// }

// int main(void)
// {
// 	m();
// 	ft_m();
// 	return(0);
// }