/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: charangu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/27 11:51:36 by charangu          #+#    #+#             */
/*   Updated: 2017/11/27 12:24:54 by charangu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_putstr_fd.c"

void ft_putnbr_fd(int n, int fd)
{
	if (n == -2147483648)
	{
		ft_putstr_fd("-2147483648", fd);
	}
	else if(n<0)
	{
		ft_putstr_fd("-", fd);
		n *= -1;
	}

	if (n<10)
	{
		ft_putchar_fd(n + '0', fd);
	}
	else
	{
		ft_putnbr_fd(n/10, fd);
		ft_putnbr_fd(n%10, fd);
	}
}

//int main()
//{
//	ft_putnbr_fd(71326, 65);
//	ft_putchar_fd('\n', 66);
//	ft_putnbr_fd(-192394, 67);
//	ft_putchar_fd('\n', 68);
//	ft_putnbr_fd(-2147483648, 69);
//	ft_putchar_fd('\n',70);
//	ft_putnbr(7812460144618734);
//}
