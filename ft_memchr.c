
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 13:11:26 by cristi            #+#    #+#             */
/*   Updated: 2017/09/25 17:22:58 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memchr(const void *src, int c, size_t n)
{
	unsigned char *ft_src;

	if (src == NULL)
		return (NULL);
	ft_src = (unsigned char*) src;

	while(n > 0)
	{
		if(*ft_src++ == (unsigned char)c)
			return ((void*)(ft_src - 1));
		n--;
	}
	return (NULL);
}
