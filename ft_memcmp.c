/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 16:03:50 by cristi            #+#    #+#             */
/*   Updated: 2017/09/25 18:12:45 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_memcmp(const void *str1, const void *str2, size_t n)
{
	unsigned char *ft_str1;
	unsigned char *ft_str2;

	ft_str1 = (unsigned char*)str1;
	ft_str2 = (unsigned char*)str2;
	while (n > 0 && *ft_str1 == *ft_str2)
	{
		ft_str1++;
		ft_str2++;
		n--;
	}
	if (n == 0)
		return (0);
	else
		return (*ft_str1 - *ft_str2);
}
