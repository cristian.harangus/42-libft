/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.old.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 16:03:50 by cristi            #+#    #+#             */
/*   Updated: 2017/09/25 18:12:59 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Compare two blocks of memory
// Compares the first num bytes of the block of memory pointed by ptr1 to the first
// 		num bytes pointed by ptr2, returning zero if they all match or a value
// 		different from zero representing which is greater if they do not.
// Notice that, unlike strcmp, the function does not stop comparing after finding a
// 		null character.


#include "libft.h"
// char		*randstring(size_t length);
// int			ft_memcmp(const void *str1, const void *str2, size_t n);
// void		ft_cmp_rez(char *buffer1, char *buffer2);

int			ft_memcmp(const void *str1, const void *str2, size_t n)
{
	unsigned char *ft_str1;
	unsigned char *ft_str2;

	ft_str1 = (unsigned char*)str1;
	ft_str2 = (unsigned char*)str2;
	while (n > 0 && *ft_str1 == *ft_str2)
	{
		ft_str1++;
		ft_str2++;
		n--;
	}
	if (n == 0)
		return (0);
	else
		return (*ft_str1 - *ft_str2);
}


void	ft_cmp_rez(char *buffer1, char *buffer2)
{
	int n;
	int ft_n;

	n = memcmp (buffer1, buffer2, (int)sizeof(buffer1));
	ft_n = ft_memcmp ( buffer1, buffer2, (int)sizeof(buffer1));

	if (n > 0)
	printf ("'%s' is greater than '%s'.\n", buffer1, buffer2);
	else if (n < 0)
		printf ("'%s' is less than '%s'.\n", buffer1, buffer2);
	else
		printf ("'%s' is the same as '%s'.\n", buffer1, buffer2);

	if(ft_n > 0)
		printf ("'%s' is greater than '%s'.\n", buffer1, buffer2);
	else if (ft_n < 0)
		printf ("'%s' is less than '%s'.\n", buffer1, buffer2);
	else
		printf ("'%s' is the same as '%s'.\n", buffer1, buffer2);
	// return(0);
}

static char *rand_string(char *str, size_t size)
{
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";
    if (size) {
        --size;
        for (size_t n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}

// char *randstring(size_t length)
// {
// 	static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!";        
// 	char *randomString = NULL;
// 	if (length)
// 	{
// 		randomString = malloc(sizeof(char) * (length +1));
// 		if (randomString)
// 		{
// 			for (int n = 0; n < (int)length; n++)
// 			{
// 				int key = rand() % (int)(sizeof(charset) -1);
// 				randomString[n] = charset[key];
// 			}
// 			randomString[length] = '\0';
// 		}
// 	}
// 	return randomString;
// }

int main ()
{

	char buffer1[100];
	char buffer2[100];

	// buffer1 = randstring(15);
	// buffer2 = randstring(15);
	// buffer1 = "DWgaO23tP12df0";
	// buffer2 = "DWGAO23TP12DF0";

	int i, n = 3;
	time_t t;
	
	/* Intializes random number generator */
	// srand((unsigned) time(&t));
 
	/* Print 5 random numbers from 0 to 49 */
	for( i = 0 ; i < n ; i++ ) 
	{
		srand((unsigned) time(&t));
		rand_string(buffer1, rand() % 50);
		rand_string(buffer2, rand() % 50);
		// printf("%d\n", rand() % 50);
		ft_cmp_rez(buffer1, buffer2);
		write(1, "\n",1 );
	}
	return(0);
}
