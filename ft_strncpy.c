/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 12:57:18 by cristi            #+#    #+#             */
/*   Updated: 2017/09/27 13:51:10 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strncpy(char *dest, const char *src, size_t len)
{
	char *temp;

	if (dest == NULL || src == NULL)
		return (NULL);
	temp = dest;
	while (*src != '\0' && len > 0)
	{
		*dest++ = *src++;
		len--;
	}
	while(len > 0)
	{
		*dest++ = '\0';
		len--;
	}
	*dest = '\0';
	return (temp);
}