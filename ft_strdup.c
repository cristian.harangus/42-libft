/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cristi <cristi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 11:51:45 by cristi            #+#    #+#             */
/*   Updated: 2017/09/27 12:09:32 by cristi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(const char *s1)
{
	char *temp;

	temp = (char *) malloc(sizeof(char) * (ft_strlen(s1) + 1));
	if(temp == NULL)
		return(NULL);
	
	return(ft_strcpy(temp, s1));
}
